package si.uni_lj.fri.pbd.miniapp3.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.FavoritesViewModel;
import timber.log.Timber;

public class DetailsActivity extends AppCompatActivity {

    public static final String RECIPE_ID = "recipe_id";
    public static final String CALLER_CLASS = "caller_class";

    private RecipeDetailsIM recipeDetailsIM = null;
    private String recipeId;

    private RestAPI restAPI;

    private FavoritesViewModel favoritesVM;

    private ConstraintLayout container;
    private TextView msg;
    private MaterialProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // init the restAPI
        restAPI = ServiceGenerator.createService(RestAPI.class);

        RecyclerViewAdapter.Caller callerClass = null;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // get recipeId and callerClass from intent
            recipeId = extras.getString(RECIPE_ID);
            callerClass = (RecyclerViewAdapter.Caller) extras.get(CALLER_CLASS);
        }

        favoritesVM = new ViewModelProvider(this).get(FavoritesViewModel.class);
        setObservers();

        if (callerClass == RecyclerViewAdapter.Caller.SearchFragment) {
            favoritesVM.getRecipeById(recipeId);
        } else if (callerClass == RecyclerViewAdapter.Caller.FavoritesFragment) {
            favoritesVM.getRecipeById(recipeId);
        }

        // hide the view until data is loaded
        container = findViewById(R.id.details_container);
        container.setVisibility(View.INVISIBLE);

        // textview for displaying connectivity errors
        msg = findViewById(R.id.details_msg_text);

        progressBar = findViewById(R.id.detail_progressBar);
    }

    private void setObservers() {
        favoritesVM.getQueryResult().observe(this, new Observer<List<RecipeDetails>>() {
            @Override
            public void onChanged(List<RecipeDetails> recipeDetailsList) {
                if(recipeDetailsList.size() > 0) {
                    RecipeDetails recipeDetails = recipeDetailsList.get(0);
                    recipeDetailsIM = Mapper.mapRecipeDetailsToRecipeDetailsIm(true , recipeDetails);
                    setRecipeDetailsData();
                } else {
                    fetchRecipeDetailsFromAPI(recipeId);
                }
            }
        });
    }

    private void fetchRecipeDetailsFromAPI(String recipeId) {
        progressBar.setVisibility(View.VISIBLE);
        Call<RecipesByIdDTO> call = restAPI.getRecipesById(recipeId);
        call.enqueue(new Callback<RecipesByIdDTO>() {
            @Override
            public void onResponse(Call<RecipesByIdDTO> call, Response<RecipesByIdDTO> response) {
                if (response.isSuccessful()) {
                    //RecipeDetailsDTO recipeDetailsDTO = response.body().getRecipeDetails().get(0);
                    recipeDetailsIM = Mapper.mapRecipeDetailsDtoToRecipeDetailsIm(false,response.body().getRecipeDetails().get(0));
                    progressBar.setVisibility(View.GONE);
                    setRecipeDetailsData();
                }
            }

            @Override
            public void onFailure(Call<RecipesByIdDTO> call, Throwable t) {
                Toast.makeText(getApplication(), "Could not connect to server.", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                msg.setText("Please, connect to a network and return to recipe search");
                msg.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setRecipeDetailsData() {
        ImageView detailsImageView = findViewById(R.id.details_image_view);
        if (recipeDetailsIM.getFavorite()){
            Glide.with(this).load(recipeDetailsIM.getStrMealThumb()).onlyRetrieveFromCache(true).into(detailsImageView);
        } else {
            Glide.with(this).load(recipeDetailsIM.getStrMealThumb()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(detailsImageView);
        }

        TextView mealName = findViewById(R.id.details_meal_name_txt);
        mealName.setText(recipeDetailsIM.getStrMeal());

        TextView mealArea = findViewById(R.id.details_meal_area_txt);
        mealArea.setText(recipeDetailsIM.getStrArea());

        TextView mealIngredients = findViewById(R.id.details_meal_ingredients_txt);
        mealIngredients.setText(recipeDetailsIM.getIngredientsString());

        TextView mealMeasurements = findViewById(R.id.details_meal_measures_txt);
        mealMeasurements.setText(recipeDetailsIM.getMeasurementsString());

        TextView mealInstructions = findViewById(R.id.details_meal_instructions_txt);
        mealInstructions.setText(recipeDetailsIM.getStrInstructions());

        ImageButton favoritesButton = findViewById(R.id.favoritesButton);
        if (recipeDetailsIM.getFavorite()){
            favoritesButton.setImageResource(R.drawable.ic_favorite_black);
        } else {
            favoritesButton.setImageResource(R.drawable.ic_favorite_border_black);
        }
        favoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recipeDetailsIM.getFavorite()){
                    favoritesVM.deleteRecipe(recipeId);
                    favoritesButton.setImageResource(R.drawable.ic_favorite_border_black);
                    Toast.makeText(getApplicationContext(), "Recipe deleted from DB",Toast.LENGTH_LONG).show();
                } else {
                    RecipeDetails recipeDetails = Mapper.mapRecipeDetailsIMToRecipeDetails(true, recipeDetailsIM);
                    favoritesVM.insertRecipe(recipeDetails);
                    favoritesButton.setImageResource(R.drawable.ic_favorite_black);
                    Toast.makeText(getApplicationContext(), "Recipe added to DB",Toast.LENGTH_LONG).show();
                }
            }
        });

        container.setVisibility(View.VISIBLE);
    }

}
