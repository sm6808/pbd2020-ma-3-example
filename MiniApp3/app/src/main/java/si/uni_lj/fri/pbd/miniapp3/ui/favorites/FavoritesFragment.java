package si.uni_lj.fri.pbd.miniapp3.ui.favorites;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;

public class FavoritesFragment extends Fragment {

    private FavoritesViewModel favoritesViewModel;

    private RecyclerViewAdapter recyclerViewAdapter;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance(String param1, String param2) {
        FavoritesFragment fragment = new FavoritesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Bind the fragment to the ViewModel
        favoritesViewModel = new ViewModelProvider(this).get(FavoritesViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set up the recycler
        initRecycler();

        // Set up the observer
        initObserver();
    }

    private void initObserver() {
        favoritesViewModel.getAllRecipes().observe(getViewLifecycleOwner(), new Observer<List<RecipeDetails>>() {
            @Override
            public void onChanged(List<RecipeDetails> recipeDetailsList) {
                List<RecipeSummaryIM> recipeSummaryIMList = new ArrayList<>();
                for (RecipeDetails recipeDetails: recipeDetailsList) {
                    RecipeSummaryIM recipeSummaryIM = Mapper.mapRecipeDetailsToRecipeSummaryIm(recipeDetails);
                    recipeSummaryIMList.add(recipeSummaryIM);
                }

                recyclerViewAdapter.setRecipeList(recipeSummaryIMList);
            }
        });
    }

    private void initRecycler() {
        recyclerViewAdapter = new RecyclerViewAdapter(getContext(), RecyclerViewAdapter.Caller.FavoritesFragment);

        RecyclerView recyclerView = getView().findViewById(R.id.recyclerView_favorites);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(recyclerViewAdapter);
    }
}
