package si.uni_lj.fri.pbd.miniapp3.models;

import java.util.ArrayList;
import java.util.List;

public class RecipeDetailsIM {

    private Boolean isFavorite;
    private String idMeal;
    private String strMeal;
    private String strCategory;
    private String strArea;
    private String strInstructions;
    private String strMealThumb;
    private String strYoutube;
    private String strIngredient1;
    private String strIngredient2;
    private String strIngredient3;
    private String strIngredient4;
    private String strIngredient5;
    private String strIngredient6;
    private String strIngredient7;
    private String strIngredient8;
    private String strIngredient9;
    private String strIngredient10;
    private String strIngredient11;
    private String strIngredient12;
    private String strIngredient13;
    private String strIngredient14;
    private String strIngredient15;
    private String strIngredient16;
    private String strIngredient17;
    private String strIngredient18;
    private String strIngredient19;
    private String strIngredient20;
    private String strMeasure1;
    private String strMeasure2;
    private String strMeasure3;
    private String strMeasure4;
    private String strMeasure5;
    private String strMeasure6;
    private String strMeasure7;
    private String strMeasure8;
    private String strMeasure9;
    private String strMeasure10;
    private String strMeasure11;
    private String strMeasure12;
    private String strMeasure13;
    private String strMeasure14;
    private String strMeasure15;
    private String strMeasure16;
    private String strMeasure17;
    private String strMeasure18;
    private String strMeasure19;
    private String strMeasure20;
    private String strSource;

    public RecipeDetailsIM() {}

    public RecipeDetailsIM(Boolean isFavorite, String idMeal, String strMeal, String strCategory, String strArea,
                           String strInstructions, String strMealThumb, String strYoutube, String strIngredient1,
                           String strIngredient2, String strIngredient3, String strIngredient4, String strIngredient5,
                           String strIngredient6, String strIngredient7, String strIngredient8, String strIngredient9,
                           String strIngredient10, String strIngredient11, String strIngredient12, String strIngredient13,
                           String strIngredient14, String strIngredient15, String strIngredient16, String strIngredient17,
                           String strIngredient18, String strIngredient19, String strIngredient20, String strMeasure1,
                           String strMeasure2, String strMeasure3, String strMeasure4, String strMeasure5, String strMeasure6,
                           String strMeasure7, String strMeasure8, String strMeasure9, String strMeasure10, String strMeasure11,
                           String strMeasure12, String strMeasure13, String strMeasure14, String strMeasure15, String strMeasure16,
                           String strMeasure17, String strMeasure18, String strMeasure19, String strMeasure20, String strSource) {
        this.isFavorite = isFavorite;
        this.idMeal = idMeal;
        this.strMeal = strMeal;
        this.strCategory = strCategory;
        this.strArea = strArea;
        this.strInstructions = strInstructions;
        this.strMealThumb = strMealThumb;
        this.strYoutube = strYoutube;
        this.strIngredient1 = strIngredient1;
        this.strIngredient2 = strIngredient2;
        this.strIngredient3 = strIngredient3;
        this.strIngredient4 = strIngredient4;
        this.strIngredient5 = strIngredient5;
        this.strIngredient6 = strIngredient6;
        this.strIngredient7 = strIngredient7;
        this.strIngredient8 = strIngredient8;
        this.strIngredient9 = strIngredient9;
        this.strIngredient10 = strIngredient10;
        this.strIngredient11 = strIngredient11;
        this.strIngredient12 = strIngredient12;
        this.strIngredient13 = strIngredient13;
        this.strIngredient14 = strIngredient14;
        this.strIngredient15 = strIngredient15;
        this.strIngredient16 = strIngredient16;
        this.strIngredient17 = strIngredient17;
        this.strIngredient18 = strIngredient18;
        this.strIngredient19 = strIngredient19;
        this.strIngredient20 = strIngredient20;
        this.strMeasure1 = strMeasure1;
        this.strMeasure2 = strMeasure2;
        this.strMeasure3 = strMeasure3;
        this.strMeasure4 = strMeasure4;
        this.strMeasure5 = strMeasure5;
        this.strMeasure6 = strMeasure6;
        this.strMeasure7 = strMeasure7;
        this.strMeasure8 = strMeasure8;
        this.strMeasure9 = strMeasure9;
        this.strMeasure10 = strMeasure10;
        this.strMeasure11 = strMeasure11;
        this.strMeasure12 = strMeasure12;
        this.strMeasure13 = strMeasure13;
        this.strMeasure14 = strMeasure14;
        this.strMeasure15 = strMeasure15;
        this.strMeasure16 = strMeasure16;
        this.strMeasure17 = strMeasure17;
        this.strMeasure18 = strMeasure18;
        this.strMeasure19 = strMeasure19;
        this.strMeasure20 = strMeasure20;
        this.strSource = strSource;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public String getIdMeal() {
        return idMeal;
    }

    public String getStrMeal() {
        return strMeal;
    }

    public String getStrCategory() {
        return strCategory;
    }

    public String getStrArea() {
        return strArea;
    }

    public String getStrInstructions() {
        return strInstructions;
    }

    public String getStrMealThumb() {
        return strMealThumb;
    }

    public String getStrYoutube() {
        return strYoutube;
    }

    public String getStrIngredient1() {
        return strIngredient1;
    }

    public String getStrIngredient2() {
        return strIngredient2;
    }

    public String getStrIngredient3() {
        return strIngredient3;
    }

    public String getStrIngredient4() {
        return strIngredient4;
    }

    public String getStrIngredient5() {
        return strIngredient5;
    }

    public String getStrIngredient6() {
        return strIngredient6;
    }

    public String getStrIngredient7() {
        return strIngredient7;
    }

    public String getStrIngredient8() {
        return strIngredient8;
    }

    public String getStrIngredient9() {
        return strIngredient9;
    }

    public String getStrIngredient10() {
        return strIngredient10;
    }

    public String getStrIngredient11() {
        return strIngredient11;
    }

    public String getStrIngredient12() {
        return strIngredient12;
    }

    public String getStrIngredient13() {
        return strIngredient13;
    }

    public String getStrIngredient14() {
        return strIngredient14;
    }

    public String getStrIngredient15() {
        return strIngredient15;
    }

    public String getStrIngredient16() {
        return strIngredient16;
    }

    public String getStrIngredient17() {
        return strIngredient17;
    }

    public String getStrIngredient18() {
        return strIngredient18;
    }

    public String getStrIngredient19() {
        return strIngredient19;
    }

    public String getStrIngredient20() {
        return strIngredient20;
    }

    public String getStrMeasure1() {
        return strMeasure1;
    }

    public String getStrMeasure2() {
        return strMeasure2;
    }

    public String getStrMeasure3() {
        return strMeasure3;
    }

    public String getStrMeasure4() {
        return strMeasure4;
    }

    public String getStrMeasure5() {
        return strMeasure5;
    }

    public String getStrMeasure6() {
        return strMeasure6;
    }

    public String getStrMeasure7() {
        return strMeasure7;
    }

    public String getStrMeasure8() {
        return strMeasure8;
    }

    public String getStrMeasure9() {
        return strMeasure9;
    }

    public String getStrMeasure10() {
        return strMeasure10;
    }

    public String getStrMeasure11() {
        return strMeasure11;
    }

    public String getStrMeasure12() {
        return strMeasure12;
    }

    public String getStrMeasure13() {
        return strMeasure13;
    }

    public String getStrMeasure14() {
        return strMeasure14;
    }

    public String getStrMeasure15() {
        return strMeasure15;
    }

    public String getStrMeasure16() {
        return strMeasure16;
    }

    public String getStrMeasure17() {
        return strMeasure17;
    }

    public String getStrMeasure18() {
        return strMeasure18;
    }

    public String getStrMeasure19() {
        return strMeasure19;
    }

    public String getStrMeasure20() {
        return strMeasure20;
    }

    public String getStrSource() {
        return strSource;
    }

    @Override
    public String toString() {
        return "RecipeDetailsIM {" +
                "idMeal='" + idMeal + '\'' +
                ", strMeal='" + strMeal + '\'' +
                ", strCategory='" + strCategory + '\'' +
                ", strArea='" + strArea + '\'' +
                ", strInstructions='" + strInstructions + '\'' +
                ", strMealThumb='" + strMealThumb + '\'' +
                ", strYoutube='" + strYoutube + '\'' +
                ", strIngredient1='" + strIngredient1 + '\'' +
                ", strIngredient2='" + strIngredient2 + '\'' +
                ", strIngredient3='" + strIngredient3 + '\'' +
                ", strIngredient4='" + strIngredient4 + '\'' +
                ", strIngredient5='" + strIngredient5 + '\'' +
                ", strIngredient6='" + strIngredient6 + '\'' +
                ", strIngredient7='" + strIngredient7 + '\'' +
                ", strIngredient8='" + strIngredient8 + '\'' +
                ", strIngredient9='" + strIngredient9 + '\'' +
                ", strIngredient10='" + strIngredient10 + '\'' +
                ", strIngredient11='" + strIngredient11 + '\'' +
                ", strIngredient12='" + strIngredient12 + '\'' +
                ", strIngredient13='" + strIngredient13 + '\'' +
                ", strIngredient14='" + strIngredient14 + '\'' +
                ", strIngredient15='" + strIngredient15 + '\'' +
                ", strIngredient16='" + strIngredient16 + '\'' +
                ", strIngredient17='" + strIngredient17 + '\'' +
                ", strIngredient18='" + strIngredient18 + '\'' +
                ", strIngredient19='" + strIngredient19 + '\'' +
                ", strIngredient20='" + strIngredient20 + '\'' +
                ", strMeasure1='" + strMeasure1 + '\'' +
                ", strMeasure2='" + strMeasure2 + '\'' +
                ", strMeasure3='" + strMeasure3 + '\'' +
                ", strMeasure4='" + strMeasure4 + '\'' +
                ", strMeasure5='" + strMeasure5 + '\'' +
                ", strMeasure6='" + strMeasure6 + '\'' +
                ", strMeasure7='" + strMeasure7 + '\'' +
                ", strMeasure8='" + strMeasure8 + '\'' +
                ", strMeasure9='" + strMeasure9 + '\'' +
                ", strMeasure10='" + strMeasure10 + '\'' +
                ", strMeasure11='" + strMeasure11 + '\'' +
                ", strMeasure12='" + strMeasure12 + '\'' +
                ", strMeasure13='" + strMeasure13 + '\'' +
                ", strMeasure14='" + strMeasure14 + '\'' +
                ", strMeasure15='" + strMeasure15 + '\'' +
                ", strMeasure16='" + strMeasure16 + '\'' +
                ", strMeasure17='" + strMeasure17 + '\'' +
                ", strMeasure18='" + strMeasure18 + '\'' +
                ", strMeasure19='" + strMeasure19 + '\'' +
                ", strMeasure20='" + strMeasure20 + '\'' +
                ", strSource='" + strSource + '\'' +
                '}';
    }

    public ArrayList<String> getAllIngredients() {
        ArrayList<String> ingredients = new ArrayList<>();
        if (strIngredient1 != null && !strIngredient1.equals("")) {
            ingredients.add(strIngredient1);
        }
        if (strIngredient2 != null && !strIngredient2.equals("")) {
            ingredients.add(strIngredient2);
        }
        if (strIngredient3 != null && !strIngredient3.equals("")) {
            ingredients.add(strIngredient3);
        }
        if (strIngredient4 != null && !strIngredient4.equals("")) {
            ingredients.add(strIngredient4);
        }
        if (strIngredient5 != null && !strIngredient5.equals("")) {
            ingredients.add(strIngredient5);
        }
        if (strIngredient6 != null && !strIngredient6.equals("")) {
            ingredients.add(strIngredient6);
        }
        if (strIngredient7 != null && !strIngredient7.equals("")) {
            ingredients.add(strIngredient7);
        }
        if (strIngredient8 != null && !strIngredient8.equals("")) {
            ingredients.add(strIngredient8);
        }
        if (strIngredient9 != null && !strIngredient9.equals("")) {
            ingredients.add(strIngredient9);
        }
        if (strIngredient10 != null && !strIngredient10.equals("")) {
            ingredients.add(strIngredient10);
        }
        if (strIngredient11 != null && !strIngredient11.equals("")) {
            ingredients.add(strIngredient11);
        }
        if (strIngredient12 != null && !strIngredient12.equals("")) {
            ingredients.add(strIngredient12);
        }
        if (strIngredient13 != null && !strIngredient13.equals("")) {
            ingredients.add(strIngredient13);
        }
        if (strIngredient14 != null && !strIngredient14.equals("")) {
            ingredients.add(strIngredient14);
        }
        if (strIngredient15 != null && !strIngredient15.equals("")) {
            ingredients.add(strIngredient15);
        }
        if (strIngredient16 != null && !strIngredient16.equals("")) {
            ingredients.add(strIngredient16);
        }
        if (strIngredient17 != null && !strIngredient17.equals("")) {
            ingredients.add(strIngredient17);
        }
        if (strIngredient18 != null && !strIngredient18.equals("")) {
            ingredients.add(strIngredient18);
        }
        if (strIngredient19 != null && !strIngredient19.equals("")) {
            ingredients.add(strIngredient19);
        }
        if (strIngredient20 != null && !strIngredient20.equals("")) {
            ingredients.add(strIngredient20);
        }

        return ingredients;
    }

    public String getIngredientsString() {
        ArrayList<String> ingredients = getAllIngredients();

        String ingredientsString = ingredients.get(0);
        for (int i = 1; i < ingredients.size(); i++) {
            ingredientsString = ingredientsString.concat(",");
            ingredientsString = ingredientsString.concat(ingredients.get(i));
        }

        return ingredientsString;
    }

    public ArrayList<String> getAllMeasurements() {
        ArrayList<String> measurements = new ArrayList<>();
        if (strMeasure1 != null && !strMeasure1.trim().equals("")) {
            measurements.add(strMeasure1);
        }
        if (strMeasure2 != null && !strMeasure2.trim().equals("")) {
            measurements.add(strMeasure2);
        }
        if (strMeasure3 != null && !strMeasure3.trim().equals("")) {
            measurements.add(strMeasure3);
        }
        if (strMeasure4 != null && !strMeasure4.trim().equals("")) {
            measurements.add(strMeasure4);
        }
        if (strMeasure5 != null && !strMeasure5.trim().equals("")) {
            measurements.add(strMeasure5);
        }
        if (strMeasure6 != null && !strMeasure6.trim().equals("")) {
            measurements.add(strMeasure6);
        }
        if (strMeasure7 != null && !strMeasure7.trim().equals("")) {
            measurements.add(strMeasure7);
        }
        if (strMeasure8 != null && !strMeasure8.trim().equals("")) {
            measurements.add(strMeasure8);
        }
        if (strMeasure9 != null && !strMeasure9.trim().equals("")) {
            measurements.add(strMeasure9);
        }
        if (strMeasure10 != null && !strMeasure10.trim().equals("")) {
            measurements.add(strMeasure10);
        }
        if (strMeasure11 != null && !strMeasure11.trim().equals("")) {
            measurements.add(strMeasure11);
        }
        if (strMeasure12 != null && !strMeasure12.trim().equals("")) {
            measurements.add(strMeasure12);
        }
        if (strMeasure13 != null && !strMeasure13.trim().equals("")) {
            measurements.add(strMeasure13);
        }
        if (strMeasure14 != null && !strMeasure14.trim().equals("")) {
            measurements.add(strMeasure14);
        }
        if (strMeasure15 != null && !strMeasure15.trim().equals("")) {
            measurements.add(strMeasure15);
        }
        if (strMeasure16 != null && !strMeasure16.trim().equals("")) {
            measurements.add(strMeasure16);
        }
        if (strMeasure17 != null && !strMeasure17.trim().equals("")) {
            measurements.add(strMeasure17);
        }
        if (strMeasure18 != null && !strMeasure18.trim().equals("")) {
            measurements.add(strMeasure18);
        }
        if (strMeasure19 != null && !strMeasure19.trim().equals("")) {
            measurements.add(strMeasure19);
        }
        if (strMeasure20 != null && !strMeasure20.trim().equals("")) {
            measurements.add(strMeasure20);
        }

        return measurements;
    }

    public String getMeasurementsString() {
        ArrayList<String> measurements = getAllMeasurements();

        String measurementsString = measurements.get(0);
        for (int i = 1; i < measurements.size(); i++) {
            measurementsString = measurementsString.concat(",");
            measurementsString = measurementsString.concat(measurements.get(i));
        }

        return measurementsString;
    }

}
