package si.uni_lj.fri.pbd.miniapp3.ui.search;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import timber.log.Timber;

public class SearchFragment extends Fragment {

    private SearchViewModel searchViewModel;

    private SpinnerAdapter spinnerAdapter;
    private RecyclerViewAdapter recyclerViewAdapter;

    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;
    private boolean hasConnection;
    private boolean firstFetch = true;

    private long lastRefreshTime;

    TextView msg;

    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        //searchViewModel.fetchIngredients();

        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);
                searchViewModel.setHasConnection(true);
                hasConnection = true;
            }

            @Override
            public void onLost(Network network) {
                super.onLost(network);
                searchViewModel.setHasConnection(false);
                hasConnection = false;
            }
        };

        connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            connectivityManager.registerDefaultNetworkCallback(networkCallback);

        //check for network connection on startup
        NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
        if (nwInfo == null || !nwInfo.isConnected()) {
            searchViewModel.setHasConnection(false);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            connectivityManager.unregisterNetworkCallback(networkCallback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        msg = view.findViewById(R.id.search_msg_text);

        initSpinner();

        initRecycler();

        initSwipeRefresh();

        initObservers();
    }

    private void initObservers() {
        searchViewModel.getIngredients().observe(getViewLifecycleOwner(), new Observer<IngredientsDTO>() {
            @Override
            public void onChanged(IngredientsDTO ingredientsDTO) {
                spinnerAdapter.setIngredients(ingredientsDTO);
            }
        });

        searchViewModel.getRecipes().observe(getViewLifecycleOwner(), new Observer<RecipesByIngredientDTO>() {
            @Override
            public void onChanged(RecipesByIngredientDTO recipesByIngredientDTO) {

                if (recipesByIngredientDTO != null && recipesByIngredientDTO.getRecipes() != null && recipesByIngredientDTO.getRecipes().size() > 0) {
                    List<RecipeSummaryDTO> recipeSummaryDTOList = recipesByIngredientDTO.getRecipes();

                    List<RecipeSummaryIM> recipeSummaryIMList = new ArrayList<>();
                    for (RecipeSummaryDTO recipeSummaryDTO : recipeSummaryDTOList) {
                        RecipeSummaryIM recipeSummaryIM = Mapper.mapRecipeSummaryDTOToRecipeSummaryIm(recipeSummaryDTO);
                        recipeSummaryIMList.add(recipeSummaryIM);
                    }

                    recyclerViewAdapter.setRecipeList(recipeSummaryIMList);

                    SwipeRefreshLayout refreshContainer = getView().findViewById(R.id.refreshContainer);
                    refreshContainer.setVisibility(View.VISIBLE);

                    TextView msg = getView().findViewById(R.id.search_msg_text);
                    msg.setVisibility(View.GONE);

                } else if (recipesByIngredientDTO == null) {
                    SwipeRefreshLayout refreshContainer = getView().findViewById(R.id.refreshContainer);
                    refreshContainer.setVisibility(View.GONE);

                    TextView msg = getView().findViewById(R.id.search_msg_text);
                    msg.setText("Please connect to a network and try again");
                    msg.setVisibility(View.VISIBLE);
                } else {
                    SwipeRefreshLayout refreshContainer = getView().findViewById(R.id.refreshContainer);
                    refreshContainer.setVisibility(View.GONE);

                    TextView msg = getView().findViewById(R.id.search_msg_text);
                    msg.setText("Sorry, no recipes for this ingredient exist");
                    msg.setVisibility(View.VISIBLE);
                }
                MaterialProgressBar progressBar = getView().findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);
            }
        });

        searchViewModel.getHasConnection().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean hasConn) {
                Spinner spinner = getView().findViewById(R.id.spinnerView);
                TextView msg = getView().findViewById(R.id.search_msg_text);
                SwipeRefreshLayout refreshContainer = getView().findViewById(R.id.refreshContainer);

                if (hasConn && firstFetch) {
                    searchViewModel.fetchIngredients();
                    firstFetch = false;

                    spinner.setVisibility(View.VISIBLE);
                    refreshContainer.setVisibility(View.VISIBLE);
                    msg.setVisibility(View.GONE);

                }
                else if (!hasConn && firstFetch) {
                    spinner.setVisibility(View.GONE);
                    refreshContainer.setVisibility(View.GONE);

                    msg.setText("Please connect to a network");
                    msg.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initSwipeRefresh() {
        lastRefreshTime = System.currentTimeMillis();

        SwipeRefreshLayout refreshContainer = getView().findViewById(R.id.refreshContainer);
        refreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                long newRefreshTime = System.currentTimeMillis();
                if (newRefreshTime - lastRefreshTime  > 5000) {
                    Timber.d("Refreshing API data");
                    if (spinnerAdapter.getCount() <= 0) {
                        searchViewModel.fetchIngredients();
                    } else {
                        IngredientDTO ingredient = (IngredientDTO) spinnerAdapter.getItem(spinnerAdapter.getPos());
                        fetchRecipes(ingredient.getStrIngredient());
                    }
                    lastRefreshTime = newRefreshTime;
                } else {
                    Toast.makeText(getContext(), "Refreshing is only available every 5 seconds", Toast.LENGTH_LONG).show();
                    Timber.d("Time till refreshing API data is available: %d", newRefreshTime-lastRefreshTime);
                }
                refreshContainer.setRefreshing(false);
            }
        });
    }

    private void initRecycler() {
        recyclerViewAdapter = new RecyclerViewAdapter(getContext(), RecyclerViewAdapter.Caller.SearchFragment);

        RecyclerView recyclerView = getView().findViewById(R.id.recyclerView_search);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void initSpinner() {
        spinnerAdapter = new SpinnerAdapter(getContext());

        Spinner spinner = getView().findViewById(R.id.spinnerView);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerAdapter.setPos(position);
                IngredientDTO ingredient = (IngredientDTO) spinnerAdapter.getItem(position);
                String name = ingredient.getStrIngredient();
                fetchRecipes(name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void fetchRecipes(String ingredientName) {
        recyclerViewAdapter.clear();

        searchViewModel.fetchRecipes(ingredientName);

        MaterialProgressBar progressBar = getView().findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
    }
}
