package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity;
import timber.log.Timber;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    // Application context
    private Context context;

    // List of recipes to display in RecycleView
    private List<RecipeSummaryIM> recipeList;

    //
    public enum Caller {SearchFragment, FavoritesFragment}
    private static Caller callerClass;

    public RecyclerViewAdapter(Context context, Caller callerClass) {
        this.context = context;
        this.callerClass = callerClass;
    }

    public void setRecipeList(List<RecipeSummaryIM> recipes) {
        this.recipeList = recipes;
        notifyDataSetChanged();
    }

    public void clear() {
        if (recipeList != null) {
            recipeList.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_grid_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // fill the ViewHolder with data
       holder.bind(recipeList.get(position),context);
    }

    @Override
    public int getItemCount() {
        return recipeList == null ? 0 : recipeList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView recipeImageView;
        private TextView recipeTextView;
        private String recipeId;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recipeImageView = itemView.findViewById(R.id.image_view);
            recipeTextView = itemView.findViewById(R.id.text_view_content);
        }

        public void bind(RecipeSummaryIM recipeSummary, Context context){
            recipeTextView.setText(recipeSummary.getStrMeal());
            Glide.with(context).load(recipeSummary.getStrMealThumb()).into(recipeImageView);
            recipeId = recipeSummary.getIdMeal();

            recipeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Timber.d("RecyclerViewAdapter.bind.onClick CALLED with recipeId: %s", recipeId);
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra(DetailsActivity.RECIPE_ID, recipeId);
                    intent.putExtra(DetailsActivity.CALLER_CLASS, callerClass);
                    context.startActivity(intent);
                }
            });
        }
    }
}
