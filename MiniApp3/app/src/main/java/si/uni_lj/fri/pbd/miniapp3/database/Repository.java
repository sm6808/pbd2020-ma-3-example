package si.uni_lj.fri.pbd.miniapp3.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class Repository {

    private LiveData<List<RecipeDetails>> allRecipes;
    private MutableLiveData<List<RecipeDetails>> queryResult = new MutableLiveData<>();

    private RecipeDao recipeDao;

    public Repository(Application application) {
        Database db = Database.getDatabase(application);
        recipeDao = db.recipeDao();

        allRecipes = recipeDao.getAllRecipes();
    }

    public void insertRecipe(RecipeDetails recipeDetails) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                recipeDao.insertRecipe(recipeDetails);
            }
        });
    }

    public void deleteRecipe(String idMeal){
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                recipeDao.deleteRecipe(idMeal);
            }
        });
    }

    public void getRecipeById(String idMeal) {
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                queryResult.postValue(recipeDao.getRecipeById(idMeal));
            }
        });
    }

    public LiveData<List<RecipeDetails>> getAllRecipes() {
        return allRecipes;
    }

    public MutableLiveData<List<RecipeDetails>> getQueryResult() {
        return queryResult;
    }
}
