package si.uni_lj.fri.pbd.miniapp3.ui.favorites;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.Repository;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class FavoritesViewModel extends AndroidViewModel {

    private LiveData<List<RecipeDetails>> allRecipes;
    private MutableLiveData<List<RecipeDetails>> queryResult;

    private Repository repository;

    public FavoritesViewModel(@NonNull Application application) {
        super(application);

        repository = new Repository(application);

        allRecipes = repository.getAllRecipes();
        queryResult = repository.getQueryResult();
    }

    public LiveData<List<RecipeDetails>> getAllRecipes() {
        return allRecipes;
    }

    public MutableLiveData<List<RecipeDetails>> getQueryResult() {
        return queryResult;
    }

    public void getRecipeById(String idMeal) {
        repository.getRecipeById(idMeal);
    }

    public void insertRecipe(RecipeDetails recipeDetails) {
        repository.insertRecipe(recipeDetails);
    }

    public void deleteRecipe(String idMeal){
        repository.deleteRecipe(idMeal);
    }
}
