package si.uni_lj.fri.pbd.miniapp3.ui.search;

import android.app.Application;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;
import timber.log.Timber;

public class SearchViewModel extends AndroidViewModel {

    private MutableLiveData<IngredientsDTO> ingredients = new MutableLiveData<>();
    private MutableLiveData<RecipesByIngredientDTO> recipes = new MutableLiveData<>();
    private MutableLiveData<Boolean> hasConnection = new MutableLiveData<>();

    private RestAPI restAPI;

    public SearchViewModel (Application application) {
        super(application);

        restAPI = ServiceGenerator.createService(RestAPI.class);

    }

    public void fetchIngredients() {
        Call<IngredientsDTO> call = restAPI.getAllIngredients();
        call.enqueue(new Callback<IngredientsDTO>() {
            @Override
            public void onResponse(Call<IngredientsDTO> call, Response<IngredientsDTO> response) {
                if (response.isSuccessful()){
                    ingredients.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<IngredientsDTO> call, Throwable t) {
                Toast.makeText(getApplication(), "Could not connect to server. Check internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void fetchRecipes(String ingredient) {
        Call<RecipesByIngredientDTO> call = restAPI.getRecipesByIngredient(ingredient);
        call.enqueue(new Callback<RecipesByIngredientDTO>() {
            @Override
            public void onResponse(Call<RecipesByIngredientDTO> call, Response<RecipesByIngredientDTO> response) {
                if (response.isSuccessful()) {
                    recipes.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<RecipesByIngredientDTO> call, Throwable t) {
                Toast.makeText(getApplication(), "Could not connect to server.", Toast.LENGTH_LONG).show();
                recipes.postValue(null);
            }
        });
    }

    public MutableLiveData<IngredientsDTO> getIngredients() {
        return ingredients;
    }

    public MutableLiveData<RecipesByIngredientDTO> getRecipes() {
        return recipes;
    }

    public MutableLiveData<Boolean> getHasConnection() {
        return hasConnection;
    }

    public void setHasConnection(Boolean hasConnectionBool) {
        this.hasConnection.postValue(hasConnectionBool);
    }
}
