package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<IngredientDTO> ingredients;

    private int pos;

    public SpinnerAdapter(Context context) {
        this.context = context;
    }

    public void setIngredients(IngredientsDTO ingredients) {
        this.ingredients = ingredients.getIngredients();
        notifyDataSetChanged();
    }

    public void clear() {
        ingredients.clear();
    }

    @Override
    public int getCount() {
        return ingredients == null ? 0 : ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients == null ? null : ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, parent, false);
        }

        IngredientDTO ingredient = (IngredientDTO) getItem(position);
        if (ingredient != null) {
            TextView nameText = convertView.findViewById(R.id.text_view_spinner_item);
            nameText.setText(ingredient.getStrIngredient());
        }

        return convertView;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
